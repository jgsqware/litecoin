## litecoin

### Dockerfile

> Dockerfile file: `./Dockerfile`

> Some part of the Dockerfile has been inspired by https://github.com/uphold/docker-litecoin-core/blob/master/0.18/Dockerfile, to understand how litecoin daemon is working 

The `Dockerfile` is created with Multi-stage build.
The build stage is optimized to speed up the build and can be speed up by storing the pgp key in the repository and importing it during the build phase, to avoid to validate against a online keyserver, which can be slow.

The final image is based on the `distroless/cc` image to reduce the attack surface as it reduce to the minimum what is inside the container.

The `distroless` images provide a **non-root** user `65532`, which is used to run the litecoin daemon.

Improvement: Image could be signed with Notary or tools like [cosign](https://github.com/sigstore/cosign).

### Kubernetes

> Helm chart under `./litecoin`

- The `litecoin` image defined in the previous step will be deployed on Kubernetes as a `StatefulSet`. 

- The `Data folder` will be mounted as a Persistent Volume.
- `Readiness probe` will be checking the port `9333` of the Litecoin Daemon

- The project will deploy as an Helm chart by the deploy stage of the gitlab-ci. 

### Gitlab-CI

> CI file: `./gitlab-ci.yaml`

Gitlab-CI is divided in 3 stages:

- **lint**: The `Dockerfile` will go through the linter [hadolint](https://github.com/hadolint/hadolint)
- **build**: 
    - The image will be built and tagged as latest on main branch or image tag will be git tag.
    - The image will be scanned via [Anchore Grype](https://github.com/anchore/grype) nad will fail the build if vulnerabilities higher than `high`, and with a fix, is found in the image. Vulnerabilities lower than `high` or with no fix will be discarded
    - The image will be pushed in the Gitlab Container Registry
- **deploy**: The image will be deployed as a `Statefulset` on a `EKS cluster` via Helm chart


## Registry Remover

> Scripts under: `./registry-remover`

This script remove registry from docker image name, if the image is not coming from gcr.io.

Both script/program read file passed as argument or data from STDIN

### Shell

```
$ ./registry-remover/registry-remover.sh ./registry-remover/images.txt 
$ cat ./registry-remover/images.txt | ./registry-remover/registry-remover.sh

Filter out images from gcr.io than remove registry from the image name

amazon/cloudwatch-agent:1.247347.6b250880
dexidp/dex:v2.27.0
jimmidyson/configmap-reload:v0.5.0
argoproj/argocd:v2.1.5
jetstack/cert-manager-cainjector:v1.5.4
jetstack/cert-manager-controller:v1.5.4
jetstack/cert-manager-webhook:v1.5.4
kiali/kiali:v1.27.0
prometheus/node-exporter:v1.1.2
prometheus/prometheus:v2.26.0
redis:6.2.4-alpine

```

### Go program

```
$ go run registry-remover/main.go registry-remover/images.txt
$ cat ./registry-remover/images.txt | go run registry-remover/main.go

Filter out images from gcr.io than remove registry from the image name

amazon/cloudwatch-agent:1.247347.6b250880
dexidp/dex:v2.27.0
jimmidyson/configmap-reload:v0.5.0
argoproj/argocd:v2.1.5
jetstack/cert-manager-cainjector:v1.5.4
jetstack/cert-manager-controller:v1.5.4
jetstack/cert-manager-webhook:v1.5.4
kiali/kiali:v1.27.0
prometheus/node-exporter:v1.1.2
prometheus/prometheus:v2.26.0
redis:6.2.4-alpine
```

## Terraform IAM role

> Terraform project under `./iam`

The terraform project in subfolder `./iam` will create:

- an empty role assumable by every user in the account
- a policy to let users to assume the role
- a group with a user, with the policy attached.

Terraform AWS provider sub-modules has been used to ease the setup of such standard configurations

- The environment (`var.env`) is configurable (possible value: `["dev","test","prod"]`)
- A suffix can be had to the default resource name to define more context ("prod-ci" -> "prod-ci-role") via the toggle `var.toggleNameContext`

For this example, the `statetf` file is stored locally, but best practice is to have centralized on a external storage (eg. s3)