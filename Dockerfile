FROM debian:stable-slim as builder
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN apt-get update -y \
  && apt-get install --no-install-recommends -y \
    gnupg=2.2.27-2 \
  && (\
    gpg --no-tty --keyserver pgp.mit.edu --recv-keys FE3348877809386C || \
    gpg --no-tty --keyserver keyserver.pgp.com --recv-keys FE3348877809386C || \
    gpg --no-tty --keyserver hkp://keyserver.ubuntu.com --recv-keys FE3348877809386C)
# Add multiple keyserver because pgp.mit.edu is not always answering
# To speed up build, we can have the key in the git repository and import it during the build

ARG LITECOIN_VERSION=0.18.1

WORKDIR /tmp

ADD https://download.litecoin.org/litecoin-"${LITECOIN_VERSION}"/linux/litecoin-"${LITECOIN_VERSION}"-x86_64-linux-gnu.tar.gz /tmp
ADD https://download.litecoin.org/litecoin-"${LITECOIN_VERSION}"/linux/litecoin-"${LITECOIN_VERSION}"-linux-signatures.asc /tmp

RUN gpg --verify litecoin-"${LITECOIN_VERSION}"-linux-signatures.asc  \
  && grep "$(sha256sum litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz | awk '{ print $1 }')" litecoin-"${LITECOIN_VERSION}"-linux-signatures.asc \
  && tar --strip=2 -xzf ./*.tar.gz -C /usr/local/bin  \
  && mkdir -p /home/litecoin/.litecoin  \
  && chmod 700 /home/litecoin/.litecoin  

# hadolint ignore=DL3006
FROM gcr.io/distroless/cc
COPY --from=builder /usr/local/bin/litecoind /usr/local/bin/ 
COPY --from=builder --chown=65532:65532 /home/litecoin/.litecoin /home/litecoin/.litecoin
WORKDIR /home/litecoin/.litecoin

# distroless has a non-root user 65532 > https://github.com/GoogleContainerTools/distroless/blob/main/base/base.bzl#L8
USER 65532
EXPOSE 9333
VOLUME /home/litecoin/.litecoin
ENTRYPOINT ["/usr/local/bin/litecoind"] 
CMD ["-datadir=/home/litecoin/.litecoin","-printtoconsole"]