#!/bin/bash 
[ $# -ge 1 ] && [ -f "$1" ] && IMAGEFILE="$1" || IMAGEFILE="-"
set -Eeuo pipefail

echo "Filter out images from gcr.io than remove registry from the image name"
echo
grep -v gcr.io "$IMAGEFILE" | sed -E 's,(^[^/]*/)(.*/.*),\2,' # if image have 3 '/', remove the content before the first '/' 
