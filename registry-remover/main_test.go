package main

import "testing"

func TestFilter(t *testing.T) {
	tests := []struct {
		name  string
		image string
		want  string
		want1 bool
	}{
		{
			name:  "Filter out images from gcr.io",
			image: "gcr.io/istio-release/pilot:1.10.4",
			want:  "",
			want1: false,
		},
		{
			name:  "Don't touch image from docker hub",
			image: "istio-release/pilot:1.10.4",
			want:  "istio-release/pilot:1.10.4",
			want1: true,
		},
		{
			name:  "Remove registry from image",
			image: "quay.io/istio-release/pilot:1.10.4",
			want:  "istio-release/pilot:1.10.4",
			want1: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := Filter(tt.image)
			if got != tt.want {
				t.Errorf("Filter() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Filter() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
