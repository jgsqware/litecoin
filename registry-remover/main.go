package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strings"
)

func Filter(image string) (string, bool) {
	if strings.Contains(image, "gcr.io") {
		return "", false
	}
	filterRgx := regexp.MustCompile(`^([^\/]*\/)(.*\/.*)$`)
	return filterRgx.ReplaceAllString(image, "$2"), true
}
// the registry to keep could be parameterized
// cobra could be used to create a proper cli for the tool.

func main() {
	var scanner *bufio.Scanner
	if len(os.Args) < 2 {
		scanner = bufio.NewScanner(os.Stdin)
	} else {
		fileHandle, _ := os.Open(os.Args[1])
		defer fileHandle.Close()
		scanner = bufio.NewScanner(fileHandle)
	}

	fmt.Println("Filter out images from gcr.io than remove registry from the image name")
	fmt.Println()
	for scanner.Scan() {
		image := scanner.Text()
		if result, ok := Filter(image); ok {
			fmt.Println(result)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Println(err)
	}
}
