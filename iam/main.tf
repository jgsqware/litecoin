terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.74.1"
    }
  }
}

provider "aws" {
  profile = "jgsqware-training"
  region  = "eu-west-1"
}

data "aws_caller_identity" "current" {}

locals {
  ci_role_name  = "${var.env}-ci%{if var.toggleNameContext}-role%{endif}"
  ci_group_name = "${var.env}-ci%{if var.toggleNameContext}-group%{endif}"
  ci_user_name  = "${var.env}-ci%{if var.toggleNameContext}-user%{endif}"
}

resource "aws_iam_user" "new_user" {
  name = local.ci_user_name
}

module "iam_assumable_role" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  version = "~> 4"

  trusted_role_arns = [
    "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root",
  ]

  create_role = true

  role_name = local.ci_role_name
}

# Known issue with this submodule, changing the name of the role will cause inconsistency. It need to be destroyed before chaming it again.
module "iam_group_with_assumable_roles_policy" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-group-with-assumable-roles-policy"
  version = "~> 4"

  name = local.ci_group_name

  assumable_roles = [
    module.iam_assumable_role.iam_role_arn,
  ]

  group_users = [
    aws_iam_user.new_user.name,
  ]
}
