variable "env" {
  type    = string
  default = "prod"
  validation {
    condition     = var.env == "prod" || var.env == "test" || var.env == "dev"
    error_message = "The accepted values for env are: prod, test, dev."
  }
}

variable "toggleNameContext" {
  type        = bool
  default     = false
  description = "(optional) Toggle the conext suffix on resource names"
}
